package org.nrg.xnat.fileManager;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import static java.nio.file.FileVisitResult.CONTINUE;

public class FileFinder extends SimpleFileVisitor<Path> implements Callable<List<Path>> {
    public static void main(String[] args)
            throws IOException {

        Path   root = Paths.get(args[0]);
        String pattern     = args[2];

        FileFinder       finder = new FileFinder(root, pattern);
        final List<Path> paths  = finder.call();

        System.out.println("Matched " + paths.size());
        System.out.println("Cool idea, bro, I'll put these in " + getAttachmentDisposition("xnat-logs-", Long.toString(new Date().getTime()), "zip"));

        for (final Path path : finder.getRelativePaths()) {
            System.out.println(path);
        }
        for (final Path path : finder.getDerelativizedPaths()) {
            System.out.println(path);
        }
    }

    private static final String ATTACHMENT_DISPOSITION = "attachment; filename=\"%s\"";
    protected static String getAttachmentDisposition(final String... parts) {
        final int    maxIndex = parts.length - 1;
        final String filename   = maxIndex == 0 ? parts[0] : StringUtils.join(ArrayUtils.subarray(parts, 0, maxIndex)) + "." + parts[maxIndex];
        return String.format(ATTACHMENT_DISPOSITION, filename);
    }

    private FileFinder(final Path root, final String pattern) {
        _matcher = FileSystems.getDefault().getPathMatcher("glob:" + pattern);
        _root = root;
        _paths = new ArrayList<>();
    }

    @Override
    public List<Path> call() throws IOException {
        Files.walkFileTree(_root, this);
        return _paths;
    }

    @Override
    public FileVisitResult visitFile(final Path path, final BasicFileAttributes attributes) {
        if (attributes.isRegularFile()) {
            final Path name = path.getFileName();
            if (name != null && _matcher.matches(name)) {
                _paths.add(_root.relativize(path));
            }
        }
        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(final Path path, final IOException exception) {
        return CONTINUE;
    }

    public List<Path> getRelativePaths() {
        return _paths;
    }

    public List<Path> getDerelativizedPaths() {
        return Lists.transform(_paths, new Function<Path, Path>() {
            @Override
            public Path apply(final Path path) {
                return _root.resolve(path);
            }
        });
    }

    private final PathMatcher _matcher;
    private final Path        _root;
    private final List<Path>  _paths;
}
